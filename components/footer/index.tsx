import React from 'react'
import { useTranslation } from 'next-i18next'

const Footer = () => {
  const { t } = useTranslation()

  return <footer>{t('footer_exemple')}</footer>
}

export default Footer
