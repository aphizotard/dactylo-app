import React from 'react'
import { useTranslation } from 'next-i18next'

const Header = () => {
  const { t } = useTranslation()
  return <div>{t('header_exemple')}</div>
}

export default Header
