import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import Head from 'next/head'
import { useTranslation } from 'next-i18next'
import Header from '@/components/header'
import Footer from '@/components/footer'

export default function Home() {
  const { t } = useTranslation('common')
  return (
    <div>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <h1>{t('welcome_msg')}</h1>
        <Header />
        <Footer />
      </main>
    </div>
  )
}

export async function getStaticProps({ locale = 'fr' }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common'])),
    },
  }
}
